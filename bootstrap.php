<?php

// bootstrap.php
require_once "vendor/autoload.php";
/*


use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// $paths = array("/path/to/entity-files");
// $paths = array("D:/xampp/htdocs/ci/phporm/src");
$paths = array("D:/xampp/htdocs/ci/ciorm/application/models/Entities");
//$paths = array("/src");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '',
    'dbname'   => 'ciorm',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

// return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

*/





$system_path = 'system';
// define ('ENVIRONMENT','development');
if (!defined('ENVIRONMENT')) define('ENVIRONMENT', 'development');

// define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
if (!defined('SELF')) define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// Path to the system directory
// define('BASEPATH', $system_path);
if (!defined('BASEPATH')) define('BASEPATH', $system_path);

// Path to the front controller (this file) directory
$change =str_replace('\\','/',dirname(__FILE__).DIRECTORY_SEPARATOR);
// define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);
// define('FCPATH', $change);
if (!defined('FCPATH')) define('FCPATH', $change);

// Name of the "system" directory
// define('SYSDIR', basename(BASEPATH));
if (!defined('SYSDIR')) define('SYSDIR', basename(BASEPATH));


// bootstrap.php
require_once "vendor/autoload.php";
// require_once FCPATH."application/config/database.php"; //load infor database
require_once FCPATH."application/config/database.php"; //load infor database



// echo $change;                        
// echo FCPATH;                         

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// $paths = array("/path/to/entity-files");
// $paths = array("D:/xampp/htdocs/ci/phporm/src");

//$paths = array("/src");
// $paths = array("D:/xampp/htdocs/ci/ciorm/application/models/Entities");
$paths = array(FCPATH."application/models/Entities");
$isDevMode = true;

// the connection configuration

// $dbParams = array(
    // 'driver'   => 'pdo_mysql',
    // 'user'     => 'root',
    // 'password' => '',
    // 'dbname'   => 'ciorm',
// );

$dbParams = array(
    'driver'   => $db['default']['dbdriver'],
    'user'     => $db['default']['username'],
    'password' => $db['default']['password'],
    'dbname'   => $db['default']['database'],
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

// return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

