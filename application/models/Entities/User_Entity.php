<?php
// Entities/User_Entity.php
/**
 * @Entity @Table(name="tb_user")
 **/
class User_Entity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
	/** @Column(type="string", nullable=true) **/
    protected $code;
	
	/** @Column(type="string", nullable=true) **/
    protected $name;
	
	/** @Column(type="integer", nullable=true, options={"default":1}) **/
    protected $stat;
	
	/** @Column(type="integer", nullable=true, options={"comment":"filter when show","default":0}) **/
    protected $order;
	
	public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
	public function getStat()
    {
        return $this->stat;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
	public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }
	public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

}