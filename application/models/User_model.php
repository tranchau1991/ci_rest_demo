<?php

class User_model extends CI_Model {
	private $em;
    public function __construct() {
        // Call the CI_model constructor
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
        $this->load->library('email');
		
		$this->load->library('Doctrine');
		$this->em = $this->doctrine->em();
		require_once (__DIR__).'/Entities/User_Entity.php';
    }
	
	public function get_user($id){
		
		$query = $this->em->createQuery("SELECT u FROM User_Entity u where u.id=".$id);
		$data = $query->getArrayResult();
		return $data;
	}
	
}