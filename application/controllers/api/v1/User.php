<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct() {
        // Call the CI_model constructor
        parent::__construct();
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
        $this->load->library('email');

        $this->load->model('user_model', 'user_model');
		
    }
	
	public function get_user_all()
	{
	}
	
	public function get_user($id)
	{
		$info_user = $this->user_model->get_user($id);
		$data = [];
		if($info_user != null){
			$data_send = $info_user[0];
			$data = [
				'success' => true,
				'data' =>$data_send
			];
		}else{
			$data = [
				'success' => false,
				'msg' =>'Not exit user'
			];
		}
		$response = $data;
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
	}
	
	public function post_user()
	{
	}
	public function put_user()
	{
	}
	public function delete_user($id)
	{
	}

}